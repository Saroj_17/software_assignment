﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Graphical_Assignment
{
    public class Rectangle:Shape
    {
        //declare variables
        int height, width;


        /// Parameter constructor        
        public Rectangle(int x, int y, int height, int width) : base(x, y)
        {
            this.height = height;
            this.width = width;
        }

        /// another parameter constructor        
        public Rectangle(int x, int y) : base(x, y)
        {

        }

        /// default constructor       
        public Rectangle()
        {
        }



        /// draw method

        public override void Draw(Graphics g, Color c, int thickness,Brush br)
        {
            Pen p = new Pen(c, thickness);
            g.DrawRectangle(p, x, y, height, width);
            g.FillRectangle(br, x, y, height, width);
        }


        /// height setter
        public void setHeight(int height)
        {
            this.height = height;
        }


        /// /// height getter

        public int getHeight()
        {
            return this.height;
        }

        //width setter
        public void setWidth(int width)
        {
            this.width = width;
        }


     
        public int getWidth()
        {
            return this.width;
        }
        



    }
}