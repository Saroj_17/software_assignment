﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;


namespace Graphical_Assignment
{
    class Square : Shape
    {
        //declare variables
        int  size;
       


        /// Parameter constructor        
        public Square( int x, int y) 
        {
           
        }

        //over-ride Shape class draw
        public override void Draw(Graphics g, Color c, int thickness,Brush br)
        {
            Pen p = new Pen(c, thickness);
            g.DrawRectangle(p, x, y, size,size);
            g.FillRectangle(br, x, y, size, size);
        }

        /// another parameter constructor        

        public Square():base()
        {


        }
        //set square area
        public void SetSquare(int len)
        {
            this.size = len;
        }

        public int GetSquare()
        {
            return this.size;
        }

       
    }
}
