﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Graphical_Assignment
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void commandToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Command Cobj = new Command();
            Cobj.MdiParent = this;
            Cobj.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("-@2021- Copy Right - Author ");

            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void homeToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void cmdHelpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Command-Help:\n" +
                "draw circle 100\n" +
                "draw rectangle 100 50\n" +
                "move 100 100\n" +
                "draw square 48\n" +
                "color red 23\n");
        }
    }

}