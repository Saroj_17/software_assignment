﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Graphical_Assignment
{
    interface ShapeInterface
    {
        //declare interface
        void SetColor(Color cl);

        void SetX(int x);

        int GetX();

        void SetY(int y);
        int GetY();
        void Draw(Graphics g, Color c, int thick,Brush br);

        


    }
}
