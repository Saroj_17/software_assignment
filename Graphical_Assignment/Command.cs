﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Graphical_Assignment
{
    public partial class Command : Form
    {
        public Command()
        {
            InitializeComponent();
            

        }
        // declare var
        Color c;
        int moveX, moveY;
        int thickness;
        Boolean drawCircle;
        Boolean drawRectangle;
        Boolean drawSquare;
        Boolean drawTriangle;




        //make list|array to create obj
        List<Circle> circleObjects;
        List<Rectangle> rectangleObjects;
        List<Square> squareObjects;
        List<Triangle> triangleObjects;

        private void button1_Click(object sender, EventArgs e)
        {

            if (txtCmd.Text =="")
            {
                MessageBox.Show("Command line cannot be null!!");
            }
            else if (txtInput.Text =="")
            {
                MessageBox.Show("Code Cannot be null!!");
            }
            else
            {
                //declare text input
                string cmd = txtCmd.Text;
                string cmd_cod = txtInput.Text;
                //split
                char[] delimiters = new char[] { '\r', '\n' };
                string[] parts = cmd_cod.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < parts.Length; i++)
                {
                    //pass data in a function
                    cmdParcer cmd1 = new cmdParcer();
                    string parsedCode = cmd1.ParserClass(cmd, parts[i], i + 1);


                    char[] code_delimiters = new char[] { ' ' };
                    string[] words = parsedCode.Split(code_delimiters, StringSplitOptions.RemoveEmptyEntries);
                    switch (words[0])           //switch word 
                    {
                        //cases to filter condition
                        case "circle":
                            Circle circle = new Circle();
                            circle.SetX(moveX);
                            circle.SetY(moveY);
                            circle.SetRadious(Convert.ToInt32(words[1]));
                            circleObjects.Add(circle);
                            drawCircle = true;
                            break;
                        case "rectangle":
                            Rectangle rectangle = new Rectangle();
                            rectangle.SetX(moveX);
                            rectangle.SetY(moveY);
                            rectangle.setHeight(Convert.ToInt32(words[1]));
                            rectangle.setWidth(Convert.ToInt32(words[2]));
                            rectangleObjects.Add(rectangle);
                            drawRectangle = true;
                            break;
                        case "square":
                            Square square = new Square();
                            square.SetX(moveX);
                            square.SetY(moveY);
                            square.SetSquare(Convert.ToInt32(words[1]));
                            squareObjects.Add(square);
                            drawSquare = true;
                            break;
                        case "triangle":
                            Triangle triangle = new Triangle();
                            triangle.setPoints(Convert.ToInt32(words[1]), Convert.ToInt32(words[2]), Convert.ToInt32(words[3]), Convert.ToInt32(words[4]), Convert.ToInt32(words[5]), Convert.ToInt32(words[6]));
                            triangleObjects.Add(triangle);
                            drawTriangle = true;
                            break;
                        case "clear":
                            circleObjects.Clear();
                            rectangleObjects.Clear();
                            squareObjects.Clear();

                            this.drawCircle = false;
                            this.drawRectangle = false;
                            this.drawSquare = false;
                            pcDrawing.Refresh();
                            txtInput.Clear();
                            break;
                        case "move":
                            this.moveX = Convert.ToInt32(words[1]);
                            this.moveY = Convert.ToInt32(words[2]);
                            break;
                        case "fillcolor":
                            //brush fill color

                            break;
                        case "reset":
                            this.moveX = 0;
                            this.moveY = 0;
                            this.thickness = 2;
                            c = Color.Black;
                            break;
                        case "color":
                            this.thickness = Convert.ToInt32(words[2]);
                            Color cs = Color.FromName(words[1]);

                            if (cs.IsKnownColor)
                            {
                                this.c = Color.FromName(char.ToUpper(words[1][0]) + words[1].Substring(1));
                            }
                            else
                            {
                                MessageBox.Show("Please Enter  Valid Color");
                            }
                            break;
                        default:
                            MessageBox.Show(parsedCode);
                            break;

                    }
                }
                pcDrawing.Refresh();

            }



        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
           
                
                
            
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            //draw shapes
            Graphics g = e.Graphics;
            Brush br = new SolidBrush(System.Drawing.Color.Red);

            if (drawCircle == true)
            {
                foreach (Circle circleObject in circleObjects)
                {
                    circleObject.Draw(g, c, thickness,br);
                }
            }
            if (drawRectangle == true)
            {
                foreach (Rectangle rectangleObject in rectangleObjects)
                {
                    rectangleObject.Draw(g, c, thickness,br);
                }
            }
            if (drawSquare == true)
            {
                foreach (Square squareObject in squareObjects)
                {
                    squareObject.Draw(g, c, thickness,br);
                }
            }
            if (drawTriangle == true)
            {
                foreach (Triangle triangleObject in triangleObjects)
                {
                    triangleObject.Draw(g, c, thickness,br);
                }
            }

        
    }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            //to load text from file
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Text Files|*.txt";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                txtInput.Text = System.IO.File.ReadAllText(ofd.FileName);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //to save text in files
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Text Files|*.txt";

            if(sfd.ShowDialog()== DialogResult.OK)
            {
                System.IO.File.WriteAllText(sfd.FileName, txtInput.Text);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Command_Load(object sender, EventArgs e)
        {
            //load object while loading interface
           
            circleObjects = new List<Circle>();
            rectangleObjects = new List<Rectangle>();
            squareObjects = new List<Square>();
            triangleObjects = new List<Triangle>();
            c = Color.Black;
            thickness = 2;
        }
    }
}


