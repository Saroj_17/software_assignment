﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphical_Assignment
{
    class cmdParcer
    {
       
        String Error;
        Boolean error;
        public String ParserClass(string command, string code, int line)
        {

            string cmd = command.ToLower();
            string cod = code.ToLower();

            switch (cmd)
            {
                case "run":
                    try
                    {
                        int temp;
                        String code_line = cod;
                        char[] code_delimiters = new char[] { ' ' };
                        String[] words = code_line.Split(code_delimiters, StringSplitOptions.RemoveEmptyEntries);
                        if (words[0] == "draw")
                        {
                            if (words[1] == "circle")
                            {
                                if (!(words.Length == 3))
                                {
                                    error = true;
                                    Error = " Error on line " + line + ", Please type correct code for 'draw circle <radious>'";
                                    return Error;
                                }
                                else if (Int32.TryParse(words[2], out temp))
                                {
                                    string shape = "circle " + words[2];
                                    return shape;
                                }
                                else
                                {
                                    return "Pls Enter the parameters in integers in-line " + line;

                                }
                            }
                            else if (words[1] == "square")
                            {
                                if (!(words.Length == 3))
                                {
                                    error = true;
                                    Error = "Error on line " + line + " Pls Type correct code for 'draw square <length>'";
                                    return Error;
                                }
                                else if (Int32.TryParse(words[2], out temp))
                                {
                                    
                                    string shape = "square " + words[2];
                                    
                                    return shape;
                                }
                                else
                                {
                                    return "Pls Enter, Parameters in integers " + line;

                                }
                            }

                            else if (words[1] == "rectangle")
                            {
                                if (!(words.Length == 4))
                                {
                                    error = true;
                                    Error = "Error on line " + line + " Pls type correct code for 'draw rectangle <length> <breath>'";

                                }
                                else if (Int32.TryParse(words[2], out temp) && Int32.TryParse(words[3], out temp))
                                {
                                    string shape = "rectangle " + words[2] + " " + words[3];
                                    return shape;
                                }
                                else
                                {
                                    return "Pls Enter, Parameters in integers " + line;

                                }
                            }
                            else if (words[1] == "triangle")
                            {
                                if (!(words.Length == 8))
                                {
                                    error = true;
                                   return Error = "Error!! in line " + line + " Pls type correct code for 'draw triangle <var>'";

                                }
                               
                                else
                                {

                                    string shape = "triangle " + words[2] + " " + words[3] + " " + words[4] + " " + words[5] + " " + words[6] + " " + words[7];
                                    return shape;
                                   

                                }
                            }
                            else
                            {
                                return "Pls Enter, Parameters in integers " + line;
                            }
                        }
                        else if (words[0] == "move")
                        {
                            if (!(words.Length == 3))
                            {
                                error = true;
                                Error = "Error in  line " + line + " Pls type correct code for 'move <X> <Y>'";
                                return Error;
                            }
                            else if (Int32.TryParse(words[1], out temp) && Int32.TryParse(words[2], out temp))
                            {
                                return code_line;
                            }
                            else
                            {
                                return "Pls Enter, Parameters in integers" + line;

                            }
                        }
                        else if (words[0] == "color")
                        {
                            if (!(words.Length == 3))
                            {
                                error = true;
                                Error = "Error in line " + line + " Pls type correct code for 'color <colorname>'";
                                return Error;
                            }
                            else if (Int32.TryParse(words[2], out temp))
                            {

                                return code_line;
                            }
                            else
                            {
                                return "Pls Enter, Parameters in integers";

                            }
                        }
                        else
                        {
                            return "Please enter the correct codes";
                        }

                    }
                    catch (IndexOutOfRangeException ex)
                    {
                        return ex.Message;

                    }
                    catch (FormatException ex)
                    {
                        return "!!Please input correct parameter!!";
                    }
                    catch (ArgumentOutOfRangeException ex)
                    {
                        return "!!Please input correct parameter!!";
                    }
                    break;
                case "clear":
                    return "clear";
                case "reset":
                    return "reset";
                default:
                    return "Please enter a command";

            }
            return null;
        }
    }
}
